'use strict';

const btnWrapper = document.querySelector('.btn-wrapper');
const btnName = document.querySelectorAll('.btn');

document.addEventListener('keydown', e => {
    [...btnWrapper.children].forEach(item => {
        item.style.backgroundColor = "black";
        if (e.key.toUpperCase() === item.textContent.toUpperCase()) {
            item.style.backgroundColor = "blue";
        }
    });
});



